import { Model, Optional } from 'sequelize';

export interface ICarte {
    id: number;
    name: string;
    image: string;
    createdAt?: Date;
    updatedAt?: Date;
}

export type ICarteInput = Optional<ICarte, 'id'>;
//export interface ITutorialOuput extends Required<ITutorial> {}

export interface CarteModel extends Model<ICarte, ICarteInput>, ICarte {}
