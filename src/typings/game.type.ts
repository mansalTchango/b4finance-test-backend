import { Model, Optional } from 'sequelize';

export interface IGame {
    id?: number;
    score: number;
    createdAt?: Date;
    updatedAt?: Date;
}

export type IGameInput = Optional<IGame, 'id'>;
//export interface ITutorialOuput extends Required<ITutorial> {}

export interface GameModel extends Model<IGame, IGameInput>, IGame {}
