import dotenv from 'dotenv';
import { Dialect } from 'sequelize/types';
import { Sequelize } from 'sequelize';

dotenv.config();

const host = process.env.MYSQL_HOST as string;
const user = process.env.MYSQL_USER as string;
const password = process.env.MYSQL_PASSWORD;
const db = process.env.MYSQL_DB as Dialect;
const socketPath = process.env.MYSQL_SOCKET;
const dialect = process.env.MYSQL_DIALECT as Dialect;

const sequelizeConnection = new Sequelize(db, user, password, {
    host: host,
    dialect: dialect,

    dialectOptions: {
        socketPath: socketPath,
        supportBigNumbers: true,
        bigNumberStrings: true,
    },

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },
});

export default sequelizeConnection;
