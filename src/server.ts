import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import db from './models';
import carteRouter from './routes/carte.routes';
import gameRouter from './routes/game.routes';
import swaggerUi from 'swagger-ui-express';
import swaggerFile from './swagger_output.json';

const app = express();

const corsOptions = {
    origin: process.env.CORSOPTIONS,
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile));

db.sequelize.sync({ alter: true });

// simple route
app.get('/', (req, res) => {
    res.json({ message: 'Welcome to backend test for B4Finance.' });
});

// routes
app.use('/api/cartes', carteRouter);
app.use('/api/games', gameRouter);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
