import db from '../models';
import { ICarte } from '../typings/carte.type';

const Carte = db.carteModel

export const getAll = async (): Promise<ICarte[]> => {
    const tutorials = Carte.findAll({raw: true});
    return tutorials;
};
