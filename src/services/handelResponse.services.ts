import { Response } from 'express';

export function handleSuccess(res: Response, message: string, data?: any) {
    return res.status(200).send({ status: 200, data: data, message: message });
}

export function handleError(res: Response, err: any) {
    return res.status(500).send({ status: 500, message: err.message });
}
