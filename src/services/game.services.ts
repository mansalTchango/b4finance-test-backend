import db from '../models';
import { GameModel, IGameInput, IGame } from '../typings/game.type';

const Game = db.gameModel

export const getBestGame = async (): Promise<GameModel | null> => {
    const game = Game.findOne({order: [['score', 'ASC']], raw: true});
    return game;
};

export const create = async (payload: IGameInput): Promise<IGame> => { 
    const game = await Game.create(payload)
    return game
}
