import { Router } from 'express';
import * as carteController from '../controllers/carte.controller';

const carteRouter = Router();

carteRouter.get('/getAll', 

    carteController.getAll
    // #swagger.tags = ['Carte']
    // #swagger.description = 'Recovery of card data from the database and transformation of the data'
);

export default carteRouter;
