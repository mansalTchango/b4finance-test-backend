import { Router } from 'express';
import * as gameController from '../controllers/game.controller';

const gameRouter = Router();

gameRouter.get('/getBestGame', 

    gameController.getBestGame
    // #swagger.tags = ['Game']
    // #swagger.description = 'Get best score'
);

gameRouter.post('/create', 

    gameController.create
    // #swagger.tags = ['Game']
    // #swagger.description = 'Insert in the database if the player wins the game'
);

export default gameRouter;
