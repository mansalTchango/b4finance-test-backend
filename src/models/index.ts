import sequelizeConnection from '../config/db.config';
import { Sequelize, Op, QueryTypes } from 'sequelize';
import carteModel from './carte.model'
import gameModel from './game.model'

sequelizeConnection
    .authenticate()
    .then((err) => {
        console.log('Connection successful');
    })
    .catch((err) => {
        console.log('Unable to connect to database', err);
    });

const db = {
    Op: Op,
    QueryTypes: QueryTypes,
    Sequelize: Sequelize,
    sequelize: sequelizeConnection,
    carteModel: carteModel,
    gameModel: gameModel
};

export default db;
