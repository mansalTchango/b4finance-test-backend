import { DataTypes, Sequelize } from 'sequelize';
import sequelizeConnection from '../config/db.config';
import { CarteModel } from '../typings/carte.type';
import allCarte from '../constants/allcarte';

const sequelize = new Sequelize('sqlite::memory:');

const Carte = sequelizeConnection.define<CarteModel>('Carte', {
    id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    name: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false,
    },
    image: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
});

(async () => {
    await sequelize.sync();
    await Carte.bulkCreate(allCarte);

  })();

export default Carte;
