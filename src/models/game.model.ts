import { DataTypes } from 'sequelize';
import sequelizeConnection from '../config/db.config';
import { GameModel } from '../typings/game.type';

const Game = sequelizeConnection.define<GameModel>('Game', {
    id: {
        primaryKey: true,
        autoIncrement: true, 
        type: DataTypes.INTEGER,
    },
    score: {
        type: DataTypes.INTEGER,
        allowNull: false,
    }
});

export default Game;
