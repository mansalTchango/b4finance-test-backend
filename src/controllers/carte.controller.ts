import { Response, Request } from 'express';
import * as carteService from '../services/carte.services';
import { ICarte } from '../typings/carte.type';
import { handleError, handleSuccess } from '../services/handelResponse.services';
import store from '../locale-store/store'


export const getAll = async (req: Request, res: Response) => {
    try {
        const allCarte = await carteService.getAll();
        const newGame = await createGame(allCarte)

        handleSuccess(res, `Succesfully get all carte`, newGame);
    } catch(error){
        const newGame = await createGame(store('Cartes'))
        handleSuccess(res, `Succesfully get all carte`, newGame);

        //console.error(error)
        //handleError(res, error);
    }
};

function randomizeList(array: ICarte[]) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

async function createGame(array: ICarte[]){
    const unionsCartes = await [...array, ...array];
    await randomizeList(unionsCartes);
    return unionsCartes;
}
