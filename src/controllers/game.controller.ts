import { Response, Request } from 'express';
import * as gameService from '../services/game.services';
import { IGame, IGameInput } from '../typings/game.type';
import { handleError, handleSuccess } from '../services/handelResponse.services';
import store from '../locale-store/store'

export const getBestGame = async (req: Request, res: Response) => {
    try {
        const game = await gameService.getBestGame();
        handleSuccess(res, `Succesfully get best score`, game);
    } catch (error) {
        
        const gameSort = store('Games').sort(compare);
        handleSuccess(res, `Succesfully get best score`, gameSort[0]);
        //console.error(error)
        //handleError(res, error);
    }
};


export const create = async (req: Request, res: Response): Promise<void> => {
    try {
        const body = req.body as IGameInput;

        if (!body.score) {
            res.status(400).send({
                message: 'Content can not be empty!',
            });
            return;
        }

        const winGame = await gameService.create(body);

        handleSuccess(res, 'Succesfully create game', winGame);
    } catch (error) {
        const body = req.body as IGameInput;
        store.add('Games', body); 
        handleSuccess(res, 'Succesfully create game');
        //console.error(error)
        //handleError(res, error);
    }
};

export function compare(a: IGame, b: IGame) {
    // Use toUpperCase() to ignore character casing
    const scoreA = a.score;
    const scoreB = b.score;
  
    let comparison = 0;
    if (scoreA > scoreB) {
      comparison = 1;
    } else if (scoreA < scoreB) {
      comparison = -1;
    }
    return comparison;
}