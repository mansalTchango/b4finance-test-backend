"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleError = exports.handleSuccess = void 0;
function handleSuccess(res, message, data) {
    return res.status(200).send({ status: 200, data: data, message: message });
}
exports.handleSuccess = handleSuccess;
function handleError(res, err) {
    return res.status(500).send({ status: 500, message: err.message });
}
exports.handleError = handleError;
