"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllbyTitle = exports.deleteById = exports.getById = exports.update = exports.create = void 0;
const models_1 = __importDefault(require("../models"));
//import Tutorial from '../models/tutorial.model';
const Tutorial = models_1.default.tutorialModel;
const Op = models_1.default.Op;
const create = (payload) => __awaiter(void 0, void 0, void 0, function* () {
    const tutorial = yield Tutorial.create(payload);
    return tutorial;
});
exports.create = create;
const update = (id, payload) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const isUpdate = yield Tutorial.update(payload, {
            where: { id: id },
        });
        if (isUpdate[0]) {
            return { status: 200, message: 'Succesfully update tutorial' };
        }
        else {
            throw new Error('error update');
        }
    }
    catch (error) {
        throw error;
    }
});
exports.update = update;
const getById = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const tutorial = yield Tutorial.findByPk(id);
    if (!tutorial) {
        // @todo throw custom error
        throw new Error('tutorial not found');
    }
    return tutorial;
});
exports.getById = getById;
const deleteById = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const isDeleted = yield Tutorial.destroy({
        where: { id: id },
    });
    return !!isDeleted;
});
exports.deleteById = deleteById;
const getAllbyTitle = (title) => __awaiter(void 0, void 0, void 0, function* () {
    const tutorials = Tutorial.findAll({ where: { title: { [Op.like]: `%${title}%` } } });
    return tutorials;
});
exports.getAllbyTitle = getAllbyTitle;
