"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.exempleTransaction = exports.getById = void 0;
const models_1 = __importDefault(require("../models"));
const User = models_1.default.userModel;
const Tutorial = models_1.default.tutorialModel;
const sequelize = models_1.default.sequelize;
const Op = models_1.default.Op;
const QueryTypes = models_1.default.QueryTypes;
const getById = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield sequelize.query('SELECT * FROM users WHERE [idUser] = :id', {
        replacements: { id: id },
        type: QueryTypes.SELECT,
    });
    if (!user.length) {
        // @todo throw custom error
        throw new Error('user not found');
    }
    return user;
});
exports.getById = getById;
const exempleTransaction = (input) => __awaiter(void 0, void 0, void 0, function* () {
    // First, we start a transaction and save it into a variable
    const t = yield sequelize.transaction();
    try {
        // Then, we do some calls passing this transaction as an option:
        const newUser = yield User.create(input, { transaction: t });
        const inputTutorial = (yield {
            title: `Tutorial of ${newUser.lastName}`,
        });
        const newTutorial = yield Tutorial.create(inputTutorial, { transaction: t });
        // If the execution reaches this line, no errors were thrown.
        // We commit the transaction.
        yield t.commit();
        const result = { newUser: newUser, newTutorial: newTutorial };
        return result;
    }
    catch (error) {
        // If the execution reaches this line, an error was thrown.
        // We rollback the transaction.
        yield t.rollback();
        throw error;
    }
});
exports.exempleTransaction = exempleTransaction;
