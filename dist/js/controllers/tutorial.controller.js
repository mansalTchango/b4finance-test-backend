"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllbyTitle = exports.deleteById = exports.getById = exports.update = exports.create = void 0;
const service = __importStar(require("../services/tutorial.services"));
const handelResponse_services_1 = require("../services/handelResponse.services");
const create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const body = req.body;
        if (!body.title) {
            res.status(400).send({
                message: 'Content can not be empty!',
            });
            return;
        }
        const tutorial = body;
        const newTutorial = yield service.create(tutorial);
        (0, handelResponse_services_1.handleSuccess)(res, 'Succesfully create tutorial', newTutorial);
    }
    catch (error) {
        (0, handelResponse_services_1.handleError)(res, error);
    }
});
exports.create = create;
const update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const params = req.params;
        const body = req.body;
        const id = params.id;
        const tutorial = body;
        const updateTutorial = yield service.update(id, tutorial);
        (0, handelResponse_services_1.handleSuccess)(res, updateTutorial.message);
    }
    catch (error) {
        (0, handelResponse_services_1.handleError)(res, error);
    }
});
exports.update = update;
const getById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const params = req.params;
        const id = params.id;
        const tutorial = yield service.getById(id);
        (0, handelResponse_services_1.handleSuccess)(res, 'Succesfully get tutorial', tutorial);
    }
    catch (error) {
        (0, handelResponse_services_1.handleError)(res, error);
    }
});
exports.getById = getById;
const deleteById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const params = req.params;
        const id = params.id;
        const isDelete = yield service.deleteById(id);
        (0, handelResponse_services_1.handleSuccess)(res, `delete tutorial id: ${id} is ${isDelete}`, { isDelete: isDelete });
    }
    catch (error) {
        (0, handelResponse_services_1.handleError)(res, error);
    }
});
exports.deleteById = deleteById;
const getAllbyTitle = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const params = req.params;
        const title = params.title;
        const allTutorial = yield service.getAllbyTitle(title);
        (0, handelResponse_services_1.handleSuccess)(res, `Succesfully get all tutorials`, allTutorial);
    }
    catch (error) {
        (0, handelResponse_services_1.handleError)(res, error);
    }
});
exports.getAllbyTitle = getAllbyTitle;
