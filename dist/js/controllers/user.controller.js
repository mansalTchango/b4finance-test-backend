"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.exempleTransaction = exports.getById = void 0;
const service = __importStar(require("../services/users.services"));
const handelResponse_services_1 = require("../services/handelResponse.services");
const getById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const params = req.params;
        const id = params.id;
        const user = yield service.getById(id);
        (0, handelResponse_services_1.handleSuccess)(res, 'Succesfully get user', user);
    }
    catch (error) {
        (0, handelResponse_services_1.handleError)(res, error);
    }
});
exports.getById = getById;
const exempleTransaction = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const body = req.body;
        const user = body;
        const newUser = yield service.exempleTransaction(user);
        (0, handelResponse_services_1.handleSuccess)(res, 'Succesfully exempleTransaction', newUser);
    }
    catch (error) {
        (0, handelResponse_services_1.handleError)(res, error);
    }
});
exports.exempleTransaction = exempleTransaction;
