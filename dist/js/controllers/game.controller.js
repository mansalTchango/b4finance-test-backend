"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.compare = exports.create = exports.getBestGame = void 0;
const gameService = __importStar(require("../services/game.services"));
const handelResponse_services_1 = require("../services/handelResponse.services");
const store_1 = __importDefault(require("../locale-store/store"));
const getBestGame = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const game = yield gameService.getBestGame();
        (0, handelResponse_services_1.handleSuccess)(res, `Succesfully get best score`, game);
    }
    catch (error) {
        const gameSort = (0, store_1.default)('Games').sort(compare);
        (0, handelResponse_services_1.handleSuccess)(res, `Succesfully get best score`, gameSort[0]);
        //console.error(error)
        //handleError(res, error);
    }
});
exports.getBestGame = getBestGame;
const create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const body = req.body;
        if (!body.score) {
            res.status(400).send({
                message: 'Content can not be empty!',
            });
            return;
        }
        const winGame = yield gameService.create(body);
        (0, handelResponse_services_1.handleSuccess)(res, 'Succesfully create game', winGame);
    }
    catch (error) {
        const body = req.body;
        store_1.default.add('Games', body);
        (0, handelResponse_services_1.handleSuccess)(res, 'Succesfully create game');
        //console.error(error)
        //handleError(res, error);
    }
});
exports.create = create;
function compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const scoreA = a.score;
    const scoreB = b.score;
    let comparison = 0;
    if (scoreA > scoreB) {
        comparison = 1;
    }
    else if (scoreA < scoreB) {
        comparison = -1;
    }
    return comparison;
}
exports.compare = compare;
