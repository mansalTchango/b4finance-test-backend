"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//import module
const store2_1 = __importDefault(require("store2"));
//Setting store key and data
(0, store2_1.default)('Profile', { name: 'Manish', age: 27, job: 'Developer' });
//Setting multiple store key and data
store2_1.default.setAll({ name: 'Manish', userName: 'Manntrix' });
//Get single key data 
console.log((0, store2_1.default)('Profile'));
//Get userName key from multi key data
console.log((0, store2_1.default)('userName'));
//Get all declared key 
console.log(store2_1.default.getAll());
//Get all keys in array
console.log(store2_1.default.keys());
exports.default = store2_1.default;
