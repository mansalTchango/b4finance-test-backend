"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//import module
const store2_1 = __importDefault(require("store2"));
const allcarte_1 = __importDefault(require("../constants/allcarte"));
(0, store2_1.default)('Cartes', allcarte_1.default);
(0, store2_1.default)('Games', []);
exports.default = store2_1.default;
