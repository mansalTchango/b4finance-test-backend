"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const sequelize_1 = require("sequelize");
dotenv_1.default.config();
const host = process.env.MYSQL_HOST;
const user = process.env.MYSQL_USER;
const password = process.env.MYSQL_PASSWORD;
const db = process.env.MYSQL_DB;
const socketPath = process.env.MYSQL_SOCKET;
const dialect = process.env.MYSQL_DIALECT;
const sequelizeConnection = new sequelize_1.Sequelize(db, user, password, {
    host: host,
    dialect: dialect,
    dialectOptions: {
        socketPath: socketPath,
        supportBigNumbers: true,
        bigNumberStrings: true,
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },
});
exports.default = sequelizeConnection;
