"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const allCarte = [
    {
        id: 1,
        name: 'poison',
        image: 'https://i.pinimg.com/564x/12/d6/3c/12d63c8377263c2461cc736d5ad5a198.jpg'
    },
    {
        id: 2,
        name: 'power',
        image: 'https://i.pinimg.com/564x/d0/2a/91/d02a91f8708a4bc9a8eff5df3b323d95.jpg'
    },
    {
        id: 3,
        name: 'black',
        image: 'https://i.pinimg.com/564x/e6/4a/02/e64a027869607f0a7d936e19115dfac4.jpg'
    },
    {
        id: 4,
        name: 'eagle',
        image: 'https://i.pinimg.com/564x/31/f9/f4/31f9f4a6f6c685db6ccbf95c6caa9ab2.jpg'
    },
    {
        id: 5,
        name: 'joker',
        image: 'https://i.pinimg.com/564x/b2/e0/04/b2e0044f31e41cdc1502b38c70d603e5.jpg'
    },
    {
        id: 6,
        name: 'king',
        image: 'https://i.pinimg.com/564x/20/01/05/2001057c9a93fb065e721455f93e1b25.jpg'
    },
    {
        id: 7,
        name: 'jade',
        image: 'https://i.pinimg.com/564x/6b/9a/3e/6b9a3e9b27f0fee24763e009db38abfc.jpg'
    },
    {
        id: 8,
        name: 'kakashi',
        image: 'https://i.pinimg.com/564x/99/63/fa/9963fae0907d3ba21bb5f071b3e5d98a.jpg'
    }
];
exports.default = allCarte;
