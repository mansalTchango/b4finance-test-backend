"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db_config_1 = __importDefault(require("../config/db.config"));
const User = db_config_1.default.define('User', {
    // Model attributes are defined here
    idUser: {
        primaryKey: true,
        autoIncrement: true,
        type: sequelize_1.DataTypes.INTEGER,
    },
    firstName: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    lastName: {
        type: sequelize_1.DataTypes.STRING(500),
        allowNull: false,
    },
    number: {
        type: sequelize_1.DataTypes.INTEGER,
    },
    adresse: {
        type: sequelize_1.DataTypes.STRING(1000),
    },
}, {
// Other model options go here
});
exports.default = User;
