"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db_config_1 = __importDefault(require("../config/db.config"));
const allcarte_1 = __importDefault(require("../constants/allcarte"));
const sequelize = new sequelize_1.Sequelize('sqlite::memory:');
const Carte = db_config_1.default.define('Carte', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.INTEGER,
    },
    name: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    image: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
    },
});
(() => __awaiter(void 0, void 0, void 0, function* () {
    yield sequelize.sync();
    yield Carte.bulkCreate(allcarte_1.default);
}))();
exports.default = Carte;
