"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db_config_1 = __importDefault(require("../config/db.config"));
const sequelize = new sequelize_1.Sequelize('sqlite::memory:');
const Carte = db_config_1.default.define('Carte', {
    id: {
        primaryKey: true,
        type: sequelize_1.DataTypes.INTEGER,
    },
    name: {
        primaryKey: true,
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    image: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
    },
});
(() => __awaiter(void 0, void 0, void 0, function* () {
    yield sequelize.sync();
    yield Carte.bulkCreate([{
            id: 1,
            name: 'poison',
            image: 'https://i.pinimg.com/564x/12/d6/3c/12d63c8377263c2461cc736d5ad5a198.jpg'
        },
        {
            id: 2,
            name: 'power',
            image: 'https://i.pinimg.com/564x/d0/2a/91/d02a91f8708a4bc9a8eff5df3b323d95.jpg'
        },
        {
            id: 3,
            name: 'black',
            image: 'https://i.pinimg.com/564x/e6/4a/02/e64a027869607f0a7d936e19115dfac4.jpg'
        },
        {
            id: 4,
            name: 'eagle',
            image: 'https://i.pinimg.com/564x/31/f9/f4/31f9f4a6f6c685db6ccbf95c6caa9ab2.jpg'
        },
        {
            id: 5,
            name: 'joker',
            image: 'https://i.pinimg.com/564x/b2/e0/04/b2e0044f31e41cdc1502b38c70d603e5.jpg'
        },
        {
            id: 6,
            name: 'king',
            image: 'https://i.pinimg.com/564x/20/01/05/2001057c9a93fb065e721455f93e1b25.jpg'
        },
        {
            id: 7,
            name: 'jade',
            image: 'https://i.pinimg.com/564x/6b/9a/3e/6b9a3e9b27f0fee24763e009db38abfc.jpg'
        },
        {
            id: 8,
            name: 'kakashi',
            image: 'https://i.pinimg.com/564x/99/63/fa/9963fae0907d3ba21bb5f071b3e5d98a.jpg'
        }]);
}))();
exports.default = Carte;
