"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const db_config_1 = __importDefault(require("../config/db.config"));
const sequelize_1 = require("sequelize");
const carte_model_1 = __importDefault(require("./carte.model"));
const game_model_1 = __importDefault(require("./game.model"));
db_config_1.default
    .authenticate()
    .then((err) => {
    console.log('Connection successful');
})
    .catch((err) => {
    console.log('Unable to connect to database', err);
});
const db = {
    Op: sequelize_1.Op,
    QueryTypes: sequelize_1.QueryTypes,
    Sequelize: sequelize_1.Sequelize,
    sequelize: db_config_1.default,
    carteModel: carte_model_1.default,
    gameModel: game_model_1.default
};
exports.default = db;
