"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const tutorialController = __importStar(require("../controllers/tutorial.controller"));
const tutorialRouter = (0, express_1.Router)();
tutorialRouter.post('/', tutorialController.create
// #swagger.tags = ['tuto']
// #swagger.description = 'create tuto'
);
tutorialRouter.put('/:id', tutorialController.update
// #swagger.tags = ['tuto']
// #swagger.description = 'update tuto'
);
tutorialRouter.get('/id/:id', tutorialController.getById
// #swagger.tags = ['tuto']
// #swagger.description = 'get one tuto'
);
tutorialRouter.delete('/:id', tutorialController.deleteById
// #swagger.tags = ['tuto']
// #swagger.description = 'delete tuto'
);
tutorialRouter.get('/title/:title', tutorialController.getAllbyTitle
// #swagger.tags = ['tuto']
// #swagger.description = 'get by title tuto'
);
exports.default = tutorialRouter;
