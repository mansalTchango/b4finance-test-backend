"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const models_1 = __importDefault(require("./models"));
const carte_routes_1 = __importDefault(require("./routes/carte.routes"));
const game_routes_1 = __importDefault(require("./routes/game.routes"));
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const swagger_output_json_1 = __importDefault(require("./swagger_output.json"));
const app = (0, express_1.default)();
const corsOptions = {
    origin: process.env.CORSOPTIONS,
};
app.use((0, cors_1.default)(corsOptions));
// parse requests of content-type - application/json
app.use(body_parser_1.default.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use('/doc', swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(swagger_output_json_1.default));
models_1.default.sequelize.sync({ alter: true });
// simple route
app.get('/', (req, res) => {
    res.json({ message: 'Welcome to backend test for B4Finance.' });
});
// routes
app.use('/api/cartes', carte_routes_1.default);
app.use('/api/games', game_routes_1.default);
// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
