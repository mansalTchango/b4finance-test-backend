B4finance-test Backend
===============

Projet de `test` Mansal Tchango pour B4finance 

Base de données 
===============

Vous pouvez vous munir d'une base de données MySql ou utiliser le store du backend.
A noter que l'ORM sequelize génère automatiquement les tables et insère les lignes.

Installation du serveur
===============

`Download source code` 

1 - Run commande install node
---------------

`npm insatll` 

2 - Variables d'environnement
---------------

Changez les variables d'environnement par rapport à votre bdd my sql dans le fichier `.env` 

3 - Start le serveur
---------------
`npm start`

4 - Run les tests unitaires de mocha
---------------

`npm test` (vous pouvez visualiser les résultats des tests dans le répertoire `test\reporte.html` )

5 - Module swagger
---------------

Vous disposez d'un module swagger qui effectue une documentation des APIs. 
Après avoir run le serveur, vous pouvez visualiser cette dernière : `http://localhost:8081/doc/`


