import axios from 'axios'
import {expect, assert} from 'chai'

describe('API Games', async () => {
    const url = 'http://localhost:8081/api';
    
    it('getBestGame status 200', async () => {
        const allCarte = await axios.get(url+'/games/getBestGame');
        expect(allCarte.status).to.equal(200);
    });

    it('getBestGame interface IGame', async () => {
        const allCarte: any = await axios.get(url+'/games/getBestGame');
        const firstObject = allCarte.data.data
        expect(typeof firstObject.id).to.eql('number');
        expect(typeof firstObject.score).to.eql('number');
    });

});
