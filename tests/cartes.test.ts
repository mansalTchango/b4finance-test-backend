import axios from 'axios'
import {expect, assert} from 'chai'
import {ICarte} from '../src/typings/carte.type'

describe('API Cartes', async () => {
    const url = 'http://localhost:8081/api';
    
    it('getAll status 200', async () => {
        const allCarte = await axios.get(url+'/cartes/getAll');
        expect(allCarte.status).to.equal(200);
    });

    
    it('getAll interface ICarte', async () => {
        const allCarte: any = await axios.get(url+'/cartes/getAll');
        const firstObject = allCarte.data.data[0]
        expect(typeof firstObject.id).to.eql('number');
        expect(typeof firstObject.name).to.eql("string");
        expect(typeof firstObject.image).to.eql("string");
    });

});
